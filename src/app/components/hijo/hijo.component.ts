import { Component, OnInit } from '@angular/core';
import { Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  Objeto1 = {
    nombre: "Goku",
    raza: "Saiyayin",
    tipo:"Anime"
  };

  Objeto2 = {
    nombre: "Superman",
    raza: "Kriptoniano",
    tipo:"Dibujo animado"
  }

  @Output() EmitirObjeto1 = new EventEmitter<any>();
  @Output() EmitirObjeto2 = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
    this.EmitirObjeto1.emit(this.Objeto1);
    this.EmitirObjeto2.emit(this.Objeto2)
  }

}
